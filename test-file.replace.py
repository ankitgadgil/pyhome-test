from sys import argv

script, filename = argv

prompt = "^v^: "

print "we are going to earse %r " % filename

print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN/ENTER."

raw_input("?")

print "Opening the file ..."
target = open(filename, 'w')

print "Deleting existing data ..."
target.truncate()

no_new_lines = int(raw_input("Please add number of lines to be added to file: "))

print ("Please input your data: ")
for n in range (no_new_lines):
	txt = raw_input(prompt)
	target.write(txt)
	target.write("\n")

filedisp = "python test-fileread.py %s" % filename

print """
Thanks for the cooperation, we are closing the file now.
\n If you want to view the view the new contents of the file please run the following command.
\n %r""" % filedisp