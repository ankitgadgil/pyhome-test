def break_words(sentence):
	""" This module breaks words for us """
	words = sentence.split(' ')
	return words

def sort_words(words):
	""" This module sorts the words """
	return sorted(words)

def print_first_word(words):
    """ This module prints the first word after popping it off."""
    word = words.pop(0)
    print word

def print_last_word(words):
    """ This module prints the last word after popping it off."""
    word = words.pop(-1)
    print word

def sort_sentence(sentence):
    """ This module takes in a full sentence and returns the sorted words."""
    words = break_words(sentence)
    return sort_words(words)

def print_first_and_last(sentence):
    """ This module prints the first and last words of the sentence."""
    words = break_words(sentence)
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """This module sorts the words then prints the first and last one."""
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)
