my_name = "John Doe"
my_height = 185  #in cms
my_weight = 80 #in kg
my_teeth = "white"
my_hair = "black"
my_eyes = "brown"

print "My name is %s" % my_name
print "I am %d cms tall" % my_height
print "I am %d kgs heavy" % my_weight
print "Look at my %s teeth" % my_teeth
print "My hair shine %s" % my_hair
print " and I am fond of my %s colored eyes" % my_eyes
print "so if I add %d and %d i will get %d" % (my_height, my_weight, my_height + my_weight)

x = "I am %s and my height is %d" % (my_name, my_height)

print "Some repeat info about %s : %r" % (my_name,x)

print "Need more repeat information?"
print "." * 20

h = "I am %d cms tall" % my_height
w = " %d kgs heavy" % my_weight
t = " with %s teeth" % my_teeth
ha = " and shine %s hair" % my_hairprint formater % (2.5, 3.5, 4.5, 5.5)

print formater % ("yes", True, "no", False)

print formater % (formater, formater, formater, formater)

print """
Information about %s:
\t* %s
\t* %s
\t* %s
\t* %s
""" % (my_name, h, w, t, ha)