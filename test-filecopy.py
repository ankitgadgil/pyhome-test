from sys import argv
from os.path import exists

script, filefrom, fileto = argv

print "Copying file from %s to %s" % (filefrom, fileto)
indata = open(filefrom).read()

print "%s file is %d bytes long" % (filefrom, len(indata))

print "Checking if the output file exists: %r" % exists(fileto)

print "Ready, hit RETURN to continue, CTRL-C to abort."
raw_input()

outfile = open(fileto, 'w')
outfile.write(indata)

comand = "python test-fileread.py %s" % (fileto)

print """
All done! to check the contents of the %s file
Please run the following comand
%r
""" %(fileto, comand)
